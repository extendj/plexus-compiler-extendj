package org.extendj.plexus;

/**
 * The MIT License
 *
 * Copyright (c) 2005, The Codehaus
 * ExtendJ version Copyright (c) 2017, Jesper Öqvist
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.codehaus.plexus.compiler.AbstractCompiler;
import org.codehaus.plexus.compiler.CompilerConfiguration;
import org.codehaus.plexus.compiler.CompilerException;
import org.codehaus.plexus.compiler.CompilerMessage;
import org.codehaus.plexus.compiler.CompilerOutputStyle;
import org.codehaus.plexus.compiler.CompilerResult;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.Options;
import org.extendj.ast.Problem;
import org.extendj.ast.Program;
import org.extendj.ast.TypeDecl;

public class ExtendJCompiler extends AbstractCompiler {
  // If the compile arguments should be logged.
  private static final boolean LOG_ARGS = false;

  public ExtendJCompiler() {
    super(CompilerOutputStyle.ONE_OUTPUT_FILE_PER_INPUT_FILE, ".java", ".class", null);
  }

  public CompilerResult performCompile(CompilerConfiguration config) throws CompilerException {
    List<CompilerMessage> messages = new LinkedList<CompilerMessage>();

    org.extendj.JavaCompiler.compile(new String[] { "-version" });

    Program program = new Program();
    program.initBytecodeReader(Program.defaultBytecodeReader());
    program.initJavaParser(Program.defaultJavaParser());
    Options options = program.options();
    options.initOptions();
    options.addKeyOption("-version");
    options.addKeyOption("-print");
    options.addKeyOption("-g");
    options.addKeyOption("-g:none");
    options.addKeyOption("-g:lines,vars,source");
    options.addKeyOption("-nowarn");
    options.addKeyOption("-verbose");
    options.addKeyOption("-deprecation");
    options.addKeyValueOption("-classpath");
    options.addKeyValueOption("-cp");
    options.addKeyValueOption("-sourcepath");
    options.addKeyValueOption("-bootclasspath");
    options.addKeyValueOption("-extdirs");
    options.addKeyValueOption("-d");
    options.addKeyValueOption("-encoding");
    options.addKeyValueOption("-source");
    options.addKeyValueOption("-target");
    options.addKeyOption("-help");
    options.addKeyOption("-O");
    options.addKeyOption("-J-Xmx128M");
    options.addKeyOption("-recover");
    options.addKeyOption("-XprettyPrint");
    options.addKeyOption("-XdumpTree");
    options.addKeyOption("-Xlint:unchecked");

    // UNUSED options:
    options.addKeyOption("-Xdoclint:accessibility");
    options.addKeyOption("-Xdoclint:accessibility,reference,syntax");
    options.addKeyOption("-proc:none");
    options.addKeyOption("-Xlint:all");

    List<String> args = new ArrayList<String>();

    args.add("-d");
    args.add(config.getOutputLocation());

    List<String> classpathEntries = config.getClasspathEntries();
    if (!classpathEntries.isEmpty()) {
      StringBuilder classpath = new StringBuilder();
      for (String entry : classpathEntries) {
        if (classpath.length() > 0) {
          classpath.append(":");
        }
        classpath.append(entry);
      }
      args.add("-classpath");
      args.add(classpath.toString());
    }

    if (!config.isShowWarnings()) {
      args.add("-nowarn");
    }

    // compiler-specific extra options override anything else in the config object...
    Map<String, String> extras = config.getCustomCompilerArgumentsAsMap();
    for (Map.Entry<String, String> entry : extras.entrySet()) {
      args.add(entry.getKey());
      String value = entry.getValue();
      if (value != null && !value.isEmpty()) {
        args.add(value);
      }
    }

    /*
    if (config.isDebug()) {
      // TODO: add debug option to ExtendJ.
    }

    String sourceVersion = decodeVersion(config.getSourceVersion());

    if (sourceVersion != null) {
      // TODO: handle different source versions.
    }

    String targetVersion = decodeVersion(config.getTargetVersion());

    if (targetVersion != null) {
      // TODO: handle different target versions.
    }

    if (StringUtils.isNotEmpty(config.getSourceEncoding())) {
      // TODO: handle different source encodings.
    }

    if (config.isShowDeprecation()) {
      // TODO: add deprecation option to ExtendJ.
    } else {
      // TODO: add deprecation option to ExtendJ.
    }
    */

    String[] argArray = new String[args.size()];
    args.toArray(argArray);
    options.addOptions(argArray);

    Collection<String> allSources = new ArrayList<String>();
    for (String sourceRoot : config.getSourceLocations()) {
      // annotations directory does not always exist and the below scanner fails on non existing directories
      File potentialSourceDirectory = new File(sourceRoot);
      if (potentialSourceDirectory.exists()) {
        Set<String> sources = AbstractCompiler.getSourceFilesForSourceRoot(config, sourceRoot);
        allSources.addAll(sources);
      }
    }

    if (LOG_ARGS) {
      try {
        File logfile = new File("compile.log");
        System.out.format("Writing compile arguments to file %s%n", logfile.getAbsolutePath());
        FileOutputStream f = new FileOutputStream(logfile);
        PrintStream out = new PrintStream(f);
        for (String arg : argArray) {
          out.println(arg);
        }
        for (String file : allSources) {
          out.println(file);
        }
        out.close();
        f.close();
      } catch (IOException e) {
        throw new CompilerException("Failed to write compile argument log.", e);
      }
    }

    for (String source : allSources) {
      try {
        program.addSourceFile(source);
      } catch (IOException e) {
        throw new CompilerException("Failed to parse source file", e);
      }
    }

    TypeDecl object = program.lookupType("java.lang", "Object");
    if (object.isUnknown()) {
      messages.add(new CompilerMessage("", CompilerMessage.Kind.ERROR,
          0, 0, 0, 0, "java.lang.Object is missing. The Java standard library was not found."));
      CompilerResult result = new CompilerResult();
      result.compilerMessages(messages);
      result.setSuccess(false);
      return result;
    }

    Collection<CompilationUnit> worklist = new ArrayList<CompilationUnit>();

    Iterator<CompilationUnit> iter = program.compilationUnitIterator();
    while (iter.hasNext()) {
      CompilationUnit unit = iter.next();
      worklist.add(unit);
      processCompilationUnit(program, unit, messages);
    }

    iter = program.libraryCompilationUnitIterator();
    while (iter.hasNext()) {
      CompilationUnit unit = iter.next();
      worklist.add(unit);
      processCompilationUnit(program, unit, messages);
    }

    CompilerResult compilerResult = new CompilerResult().compilerMessages(messages);

    boolean failure = false;
    for (CompilerMessage compilerMessage : messages) {
      if (compilerMessage.isError()) {
        compilerResult.setSuccess(false);
        failure = true;
        break;
      }
    }

    if (!failure) {
      for (CompilationUnit unit : worklist) {
        if (unit != null && unit.fromSource()) {
          unit.generateClassfile();
        }
      }
    }

    return compilerResult;
  }

  public String[] createCommandLine(CompilerConfiguration config) throws CompilerException {
    return null;
  }

  private void processCompilationUnit(Program program, CompilationUnit unit,
      List<CompilerMessage> messages) {
    if (unit != null && unit.fromSource()) {
      Collection<Problem> errors = unit.parseErrors();
      Collection<Problem> warnings = Collections.emptyList();
      if (errors.isEmpty()) {
        errors = unit.errors();
        warnings = unit.warnings();
      }
      if (!errors.isEmpty()) {
        for (Problem error : errors) {
          messages.add(errorMessage(error));
        }
      } else if (!warnings.isEmpty() && !program.options().hasOption("-nowarn")) {
        for (Problem warning : warnings) {
          messages.add(warningMessage(warning));
        }
      }
    }
  }

  private CompilerMessage errorMessage(Problem problem) {
    return new CompilerMessage(
        problem.fileName(),
        CompilerMessage.Kind.ERROR,
        problem.line(),
        problem.column(),
        problem.endLine(),
        problem.endColumn(),
        problem.message());
  }

  private CompilerMessage warningMessage(Problem problem) {
    return new CompilerMessage(
        problem.fileName(),
        CompilerMessage.Kind.WARNING,
        problem.line(),
        problem.column(),
        problem.endLine(),
        problem.endColumn(),
        problem.message());
  }
}

# Plexus compiler component for ExtendJ

This is a compiler component that makes it possible to use ExtendJ to compile
Maven projects.

The code is based on plexus-compiler-eclipse (MIT License).
